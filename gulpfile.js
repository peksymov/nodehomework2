const gulp = require('gulp');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const pipeline = require('readable-stream').pipeline;
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const minifyjs = require('gulp-js-minify');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');


gulp.task('t-sass', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('dist/css'));
});
gulp.task('compress', function () {
    return pipeline(
        gulp.src('src/js/*.js'),
        uglify(),
        gulp.dest('dist/js')
    );
});
exports.default = () => (
    gulp.src('src/app.css')
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest('dist'))
);



gulp.task('default', function () {
    return gulp.src('app/tmp', {read: false})
        .pipe(clean());
});



gulp.task('minify-css', () => {
    return gulp.src('styles/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist'));
});



gulp.task('minify-js', function(){
    gulp.src('./dist/a.js')
        .pipe(minifyjs())
        .pipe(gulp.dest('./dist/'));
});




gulp.task('scripts', function() {
    return gulp.src('./lib/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./dist/'));
});



exports.default = () => (
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
);


// browserSync({server: true}, function(err, bs) {
//     console.log(bs.options.urls.local);
// });
//
// browserSync({server: true}, function(err, bs) {
//     console.log(bs.options.getIn(["urls", "local"]));
// });
//
// browser-sync start --server --files "css/*.css"

gulp.watch